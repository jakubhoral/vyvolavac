/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ClientOfficer  extends Thread {

    Socket socket;
    Officer officertWindow;
    BufferedReader in;
    BufferedWriter out;
    boolean connected;
    String counterName;

    public ClientOfficer(String ip, int port, Officer officertWindow) throws IOException {
        this.socket = new Socket(ip, port);
        this.connected = true;
        this.officertWindow = officertWindow;

    }

    @Override
    public void run() {
        try {
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.out.write("officer"+"\n");            
            this.out.flush();
            System.out.println("Logging to server");
            this.counterName = in.readLine().trim();
            System.out.println("Counter "+this.counterName+": Connection estabilished.");
            while (this.connected) {
                String input = getMessage(in.readLine());
                System.out.println(input+" received from server.");
                officertWindow.getjLabel1().setText(input);
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getMessage(String message) {
        return message.trim();
    }

    public void sendToServer() throws IOException {
        System.out.println("Sending to Server: "+this.counterName);
        out.write(this.counterName + "\n");
        out.flush();
    }

}
