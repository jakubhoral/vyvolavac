/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

import java.io.IOException;
import java.util.Deque;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Vyvolavac {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Server server = new Server(9000);
            new Client();
            new Officer();
            new Informator();
        } catch (IOException ex) {
            Logger.getLogger(Vyvolavac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
