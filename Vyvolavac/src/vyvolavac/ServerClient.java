/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ServerClient extends Thread {
    Socket socket;
    BufferedReader in;
    BufferedWriter out;
    boolean clientConnected = true;
    Server server;

    public ServerClient(Socket socket, Server server) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.server = server;
        start();
    }

    @Override
    public void run() {
        System.out.println("ServerClient runned");
        while (clientConnected) {            
            try {
                String customerInfo = in.readLine();
                System.out.println("From client: "+customerInfo);
                out.write(server.addCustomerReturnRank(customerInfo)+"\n");
                out.flush();
            } catch (IOException ex) {
                Logger.getLogger(ServerClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
