/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Server extends Thread{
    ArrayList<Thread> clients= new ArrayList<Thread>();
    ArrayList<Thread> officers= new ArrayList<Thread>();
    ArrayList<ServerInformator> informator= new ArrayList<ServerInformator>();
    LinkedList<Customer> customers = new LinkedList<Customer>();
    int port;
    ServerSocket serverSocket;
    boolean serverConected;
    int actualRank=0;

    public Server(int port) throws IOException {
        this.port = port;
        serverConected=true;
        this.serverSocket = new ServerSocket(port);
        start();
    }

    @Override
    public void run() {
        
      while (serverConected){
          try {
              Socket connected = this.serverSocket.accept();
              BufferedReader br = new BufferedReader(new InputStreamReader(connected.getInputStream()));
              String type = br.readLine().trim();
              switch(type){
                  case "client":
                      ServerClient client = new ServerClient(connected, this);                      
                      clients.add(client);
                      System.out.println("Server accepted client");
                      break;
                  case "informator":
                      ServerInformator informator = new ServerInformator(connected, this);                      
                      this.informator.add(informator);
                      System.out.println("Server accepted informator");
                      break;    
                  case "officer":
                      ServerOfficer officer = new ServerOfficer(connected, this);
                      officers.add(officer);
                      System.out.println("Server accepted officer");
                      officer.start();
                      break;
                  default: System.out.println("Wrong client ");
              }
              
          } catch (IOException ex) {
              Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
          }
        
      }
    }
    
    public String addCustomerReturnRank(String procedure){
        actualRank++;
        customers.add(new Customer(actualRank, procedure));        
        return String.valueOf(actualRank);
    }
    
    public String setCounterGetClient(ServerOfficer officer){
        if (officer.getLastCustomer()!=null) customers.remove(officer.getLastCustomer());
        Customer cst = customers.stream()
                .filter(c->c.getCounter()==null)
                .findFirst()
                .get();
        cst.setCounter(officer);
        officer.setLastCustomer(cst);
        return cst.getRank()+";"+cst.getItem();
    }
    
    public String getInformations(){
        ArrayList<String> res= new ArrayList<>();
        Customer[] cst = customers.stream()
                .filter(c->c.getCounter()!=null)
                .limit(10)
                .toArray(Customer[]::new);
        String[] customersTemp =  new String[cst.length];
        for (int i = 0; i < customersTemp.length; i++) {
            customersTemp[i]=cst[i].getCounter().getCounterName()+";"+cst[i].getRank();
        }
        return String.join("&", customersTemp);
    }
    
    public static void main(String[] args) {
        try {
            new Server(9000);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void notifyAllInformators(){
        for (ServerInformator informator : informator) {
            informator.updateInformator();
        }
    }
}
