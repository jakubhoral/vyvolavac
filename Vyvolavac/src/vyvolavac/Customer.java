/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

/**
 *
 * @author Admin
 */
public class Customer {
    private int rank;
    private String item;
    private ServerOfficer counter;

    public Customer(int rank, String item) {
        this.rank = rank;
        this.item = item;
    }

    public int getRank() {
        return rank;
    }

    public String getItem() {
        return item;
    }

    public ServerOfficer getCounter() {
        return counter;
    }

    public void setCounter(ServerOfficer counter) {
        this.counter = counter;
    }
    
    
}
