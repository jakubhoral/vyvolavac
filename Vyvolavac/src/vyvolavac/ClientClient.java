/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ClientClient extends Thread {

    Socket socket;
    Client clientWindow;
    BufferedReader in;
    BufferedWriter out;
    boolean connected;

    public ClientClient(String ip, int port, Client clientWindow) throws IOException {
        this.socket = new Socket(ip, port);
        this.connected = true;
        this.clientWindow = clientWindow;

    }

    @Override
    public void run() {
        try {
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.out.write("client"+"\n");            
            this.out.flush();
            System.out.println("Logging to server");
            while (this.connected) {
                String input = getMessage(in.readLine());
                clientWindow.getjLabel1().setText(input);
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getMessage(String message) {
        return message.trim();
    }

    public void sendToServer(String item) throws IOException {
        System.out.println("Sending to Server: "+item);
        out.write(item + "\n");
        out.flush();
    }

}
