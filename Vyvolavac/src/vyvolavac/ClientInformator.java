/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.TableModel;

/**
 *
 * @author Admin
 */
public class ClientInformator extends Thread {
    Socket socket;
    Informator informatorWindow;
    BufferedReader in;
    BufferedWriter out;
    boolean connected;

    public ClientInformator(String ip, int port, Informator informatorWindow) throws IOException {
        this.socket = new Socket(ip, port);
        this.connected = true;
        this.informatorWindow = informatorWindow;

    }

    @Override
    public void run() {
        try {
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.out.write("informator"+"\n");            
            this.out.flush();
            System.out.println("Logging to server");
            while (this.connected) {
                String input = in.readLine();
                getMessage(input);
                System.out.println("Received from server: "+input);
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getMessage(String message) {
        for (int i = 0; i < message.split("&").length; i++) {
            informatorWindow.getjTable1().setValueAt(message.split("&")[i].split(";")[0], i, 0);
            informatorWindow.getjTable1().setValueAt(message.split("&")[i].split(";")[1], i, 1);
        }
        return message.trim();
    }

    public void sendToServer(String item) throws IOException {
        System.out.println("Sending to Server: "+item);
        out.write(item + "\n");
        out.flush();
    }

}
