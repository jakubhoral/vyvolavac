/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vyvolavac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ServerOfficer extends Thread{
    Socket socket;
    BufferedReader in;
    BufferedWriter out;
    boolean clientConnected = true;
    Server server;
    private static int counterNo=0;
    private int counterName;
    private Customer lastCustomer=null;

    public ServerOfficer(Socket socket, Server server) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.server = server;
        counterNo++;
        counterName=counterNo;
    }

    @Override
    public void run() {
        try {
            System.out.println("Counter "+String.valueOf(counterName)+" connected.");
            out.write(String.valueOf(counterName)+"\n");
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(ServerOfficer.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (clientConnected) {            
            try {
                String customerInfo = in.readLine().trim();
                System.out.println("Info sending to officer: "+customerInfo);
                out.write(server.setCounterGetClient(this));
                server.notifyAllInformators();
            } catch (IOException ex) {
                Logger.getLogger(ServerClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int getCounterName() {
        return counterName;
    }

    public Customer getLastCustomer() {
        return lastCustomer;
    }

    public void setLastCustomer(Customer lastCustomer) {
        this.lastCustomer = lastCustomer;
    }
    
    
}
